# Task list

Checklist for Nick:

- [ ] Managing a research archive
- [ ] OpenRefine
- [ ] ABBYY
- [ ] Jupyter Notebooks

Checklist for Vicky: 

- [X] Intro RDM
- [X] Git/GH
- [X] Intro OSF
- [ ] Intro DMP
- [X] Reproducibility