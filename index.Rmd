--- 
title: "NYU Research Data Management Class Materials"
author: "Vicky Rampin and Nick Wolf"
date: "`r Sys.Date()`"
description: "Open educational resources for learning data management best practices"
site: bookdown::bookdown_site
colorlinks: yes
documentclass: book
always_allow_html: yes
output:
  bookdown::gitbook:
    css: [style.css]
    includes:
      after_body: analytics.html
    config:
      toc:
        before: |
          <p>RDM Class Materials</p>
          <p>NYU Data Services</p>
        after: |
          <p><a href="https://gitlab.com/RLesur/bookdown-gitlab-pages" target="blank">Published with bookdown &amp; GitLab</a></p>
          <p>Original content is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</p>
          <p>Any questions, contact: <a href="mailto:vicky.rampin@nyu.edu">vicky.rampin@nyu.edu</a></p>
      edit: https://gitlab.com/nyu-dataservices/rdm-instruction/edit/main/%s
      search: yes
      download: ["pdf", "epub"]
      sharing: null
  bookdown::pdf_book:
    latex_engine: xelatex
  bookdown::epub_book: default
---

# Welcome {-}

This is a book that contains the narrative and lectures that accompany the data management workshops from [NYU Data Services](https://guides.nyu.edu/dataservices) [data management team](guides.nyu.edu/data_management). It's meant to be a persistent resource for folks to reference after coming to one of these workshops live (in-person or remote).

If you are at NYU and would like to find and maybe register for a Data Services class, you can do so here: https://nyu.libcal.com/calendar/classes?cid=1564&t=d&d=0000-00-00&cal=1564&ct=45094,4295,47942&inc=0.

You can find our general data management guide here: https://guides.nyu.edu/data_management. If you would like to set up an appointment to meet with Nick or Vicky, you can do that here: https://nyu.libcal.com/appointments/online?g=12112.

# Install Dependencies {-}

```{r install_dep}
# add any dependencies we come across
```
